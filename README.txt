The coursework included worksheets for practice (tutorials) and a 3-part 
assignment. 

Please note that the assignment sourcecode is not entirely my work, since it was 
provided half ready, with major chunks of code and reasoning missing, for it to 
be patched and completed. 

The assignment was divided into 3 major parts; 
Sequential Erlang programming, Concurrent Erlang programming, Error handling 
in Erlang programs.