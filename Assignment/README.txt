All these files were provided partially implemented, and the task was to completely
implement them, employing and manifesting the correct Erlang reasoning.

The work was done in three main stages, first focusing on the Sequential aspect 
of Erlang programming, then on the Concurrent aspect, and then on the Error 
handling. 