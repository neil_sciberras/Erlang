%%% ----------------------------------------------------------------------------
%%% @author Duncan Paul Attard
%%%
%%% Provides generic utility functions.
%%% ----------------------------------------------------------------------------
-module(util).
-export ([fetch/3, remove/3, store/4, reverse/1]).


%% -----------------------------------------------------------------------------
%% Fetches the first tuple whose Nth element is equal to the value of key from
%% the specified TupleList.
%% fetch(Key, N, TupleList) where:
%%   * Key::term() is the Key that identifies the tuple in TupleList.
%%   * N::integer() is the 1-based index of the tuple element that is to be
%%     compared to the value of Key.
%%   * TupleList::[tuple()] is the list of tuples to be searched.
%% Returns: The first occurrence of the tuple whose Nth element matches the
%%          value of Key, or false if no such tuple exists.
%% -----------------------------------------------------------------------------
fetch(Key, N, TupleList) when is_integer(N), N > 0 ->
  fetch2(Key, N, TupleList).

%*******************************************************************************
%fetch2(_Key, _N, []) -> false;
%	When the tuple list is empty, hence '[]', then obviously the key won't be found
%	and the function returns false.
%
%fetch2(Key, N, [H|_]) when Key == element(N,H) -> H;
%	When the Key if found to be in the head of the list of tuples (the first tuple), 
%	then the function returns that tuple.
%
%fetch2(Key, N, [_|T]) -> fetch2(Key, N, T).
%	If the second clause of the function was not matched, then the key should be 
%	searched for in the remaining part of the list; the tail. Thus, the 
%	function is recursively called on the Tail only. 
%*******************************************************************************
  
fetch2(_Key, _N, []) 
	-> false;
fetch2(Key, N, [H|_]) when Key == element(N,H) 
	-> H; %here, when N is larger than the size of list, because we're using guards this will not give an error, but will just not match the pattern. 
fetch2(Key, N, [_|T]) 
	-> fetch2(Key, N, T).
  
  
  
%% -----------------------------------------------------------------------------
%% Removes all tuples whose Nth element is equal to the value of key from the
%% specified TupleList.
%% remove(Key, N, TupleList) where:
%%   * Key::term() is the Key that identifies the tuple in TupleList.
%%   * N::integer() is the 1-based index of the tuple element that is to be
%%     compared to the value of Key.
%%   * TupleList::[tuple()] is the list of tuples to be processed.
%% Returns: A new TupleList that does not contain tuples whose Nth matches the
%%          value of Key, or the original TupleList if no such tuples are
%%          found.
%% -----------------------------------------------------------------------------

%*******************************************************************************
%remove2(_Key, _N, []) -> [];
%	When trying to remove tuples from an empty list, the empty list is returned.
%
%remove2(Key, N, [H|T]) when Key == element(N,H) -> remove2(Key, N, T);
%	When the key is found to be in the first tuple in the list (the head),
%	that tuple will be removed from the list, and the remove2 function is
%	applied recursively on the tail of the list. 
%
%remove2(Key, N, [H|T]) -> [H | remove2(Key, N, T)].
%	When the key is not in the head, then the head will not be removed.
%	The same list is returned, but with the tail having the remove2 function applied 
%	on it to remove any tuples that can exist with the key existing in them. 
%*******************************************************************************

remove(Key, N, TupleList) when is_integer(N), N > 0 ->
  remove2(Key, N, TupleList).

remove2(_Key, _N, []) 
	-> [];
remove2(Key, N, [H|T]) when Key == element(N,H)
	-> remove2(Key, N, T);
remove2(Key, N, [H|T])
	-> [H | remove2(Key, N, T)].
	


%% -----------------------------------------------------------------------------
%% Replaces all the tuples whose Nth element is equal to the value of Key with
%% the new tuple New from the specified TupleList.
%% store(Key, N, TupleList, New) where:
%%   * Key::term() is the Key that identifies the tuple in TupleList.
%%   * N::integer() is the 1-based index of the tuple element that is to be
%%     compared to the value of Key.
%%   * TupleList::[tuple()] is the list of tuples to be processed.
%%   * New::tuple() is the new tuple to be inserted in place of the old tuple.
%% Returns: A new TupleList wherein all tuples whose Nth element matching the
%%          value of Key are replaced with New. If no such tuples are found,
%%          TupleList is returned with New appended to its tail.
%% -----------------------------------------------------------------------------
store(Key, N, TupleList, New) when is_integer(N), N > 0, is_tuple(New) ->
  store2(Key, N, TupleList, New, false).
  
%*******************************************************************************
%store2(_Key, _N, [], New, State) when State == false -> [New];
%	If the list is empty and the state is false, meaning that it has not yet 
%	replaced any tuple in previous recursive calls, then the function 
%	returns a list with one element being the tuple 'New'.
%
%store2(Key, N, [H|T], New, _) when Key == element(N,H) -> [New|store2(Key, N, T, New, true)];
%	This function clause checks whether the first tuple contains the Key, if so, 
%	the return is set to be the list with the head equal to the 'New' tuple, 
%	and the tail being equal to the previous tail with the store2 function 
%	recursively applied on it. The state is set to true in the recursive call
% 	since a replacement already took place.
%
%store2(Key, N, [H|T], New, State) -> [H|store2(Key, N, T, New, State)];
%	When the head does not contain the Key, then it is left the same, and the 
%	tail has the store2 function applied on it, with the state being equal to 
%	the state passed as parameter to the previous function call.
%
%store2(_Key, _N, List, New, false)	-> [List ++ [New]];
%	This is the base case for when the state is false, which means that no tuples
%	were replace. This means that we need to append the list with the 'New' tuple,
%	being done by the '++' operator. 
%
%store2(_Key, _N, List, _New, true)	-> List.
%	The base case for when the state is true, meaning that a replacement took place,
%	returns the same list passed as parameter, and ends the loop of recursive calls.
%*******************************************************************************
	
store2(_Key, _N, [], New, State) when State == false 
	-> [New];
store2(Key, N, [H|T], New, _) when Key == element(N,H)
	-> [New|store2(Key, N, T, New, true)];
store2(Key, N, [H|T], New, State)
	-> [H|store2(Key, N, T, New, State)];
store2(_Key, _N, List, New, false)
	-> [List ++ [New]];
store2(_Key, _N, List, _New, true)
	-> List.

%Just fro information:
%	the operator ++, for example when doing "abc"++"d"
%	it uses foldr and the "abc" is traversed just to concatenate the other string behind it,
%	so it has order n. 
%It is cheaper to use foldl(A,B) and then reverse it.

%% -----------------------------------------------------------------------------
%% Reverses the specified list.
%% reverse(List) where:
%%   * List:list() is the list to be reversed.
%% Returns: A new List with the order of its elements reversed.
%% -----------------------------------------------------------------------------
reverse(List) ->
  reverse2(List, []).

reverse2([], L) -> L;
reverse2([H|T], L) -> reverse2(T, [H|L]).
